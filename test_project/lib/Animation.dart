import 'dart:async';
import 'dart:io';
import 'dart:ui' as ui;
import 'package:animate_do/animate_do.dart';
import 'package:flutter/animation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';

enum AnimationStatus { start, processing, finish, wait }

enum AnimationScript { one, two, three }

class MyAnimationApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Canvas Decorator',
      home: AnimationScreen(),
    );
  }
}

class AnimationScreen extends StatefulWidget {
  @override
  _AnimationScreenState createState() => _AnimationScreenState();
}

class _AnimationScreenState extends State<AnimationScreen>
    with SingleTickerProviderStateMixin {
  //score
  int numberOfScore = 2;

  int timesOfAnimationScriptOne = 1;
  int timesOfAnimationScriptTwo = 1;





  int timesOfAnimationScriptThree = 1;


  AnimationStatus animationContext1Status = AnimationStatus.start;
  AnimationStatus animationContext2Status = AnimationStatus.wait;
  AnimationStatus animationContext3Status = AnimationStatus.wait;

  AnimationStatus animationContext4Status = AnimationStatus.start;
  AnimationStatus animationContext5Status = AnimationStatus.wait;
  AnimationStatus animationContext6Status = AnimationStatus.wait;

  AnimationStatus animationContext7Status = AnimationStatus.start;
  AnimationStatus animationContext8Status = AnimationStatus.wait;
  AnimationStatus animationContext9Status = AnimationStatus.wait;

  AnimationStatus animationContext10Status = AnimationStatus.start;



  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      theme: ThemeData.light(useMaterial3: true),
      home: Scaffold(
        body: Center(
          child: Container(
            child: Column(
              children: [
                Container(
                  width: 300,
                  height: 300,
                  color: Colors.pink,
                ),
                FadeInUp(
                    from: 10,
                    child: const Square(),
                    duration: Duration(milliseconds: 500),
                    curve: Curves.easeIn),
                Container(
                  height: 200,
                  width: 300,
                  color: Colors.white,
                  child: Stack(
                    children: [
                      starOne(),
                      starTwo(),
                      starThree(),
                      StarOneWithBounce(),
                      StarTwoWithBounce(),
                      StarThreeWithBounce(),
                      StarOneWithFly(),
                      StarThreeWithFly(),
                      StarOneFlyingBackGround(),
                      StarTwoWithFly(),
                      StarTwoFlyingBackGround(),
                      StarOneWithBackgroundFinal(),
                      StarTwoWithBackgroundFinal(),
                      StarThreeWithBackgroundFinal(),
                      Positioned(
                        child: Container(
                            color: Colors.lightBlue, width: 55, height: 55),
                        top: 200 - 55,
                        left: 300 - 55,
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget starOne() {
    if (animationContext1Status == AnimationStatus.start) {
      return Align(
          alignment: Alignment.topLeft,
          child: ScaleAnimation(
            duration: Duration(milliseconds: 1000),
            child: Icon(
              Icons.favorite,
              color: Colors.grey,
              size: 100.0,
            ),
            onFinish: () {
              if(timesOfAnimationScriptOne == numberOfScore) {
                setAllFinishAnimationContextOneTwoThree(AnimationScript.one);
              } else {
                setState(() {
                  animationContext1Status = AnimationStatus.finish;
                  animationContext2Status = AnimationStatus.start;
                });
                timesOfAnimationScriptOne = timesOfAnimationScriptOne + 1;
              }
            },
          ));
    } else {
      return Align(
        alignment: Alignment.topLeft,
        child: Icon(
          Icons.favorite,
          color: Colors.grey,
          size: 100.0,
        ),
      );
    }
  }

  Widget starThree() {
    if (animationContext3Status == AnimationStatus.start) {
      return Align(
          alignment: Alignment.topRight,
          child: ScaleAnimation(
            duration: Duration(milliseconds: 1000),
            child: Icon(
              Icons.favorite,
              color: Colors.grey,
              size: 100.0,
            ),
            onFinish: () {
              if(timesOfAnimationScriptOne == numberOfScore) {
                setAllFinishAnimationContextOneTwoThree(AnimationScript.one);
              } else {
                setState(() {
                  animationContext3Status = AnimationStatus.finish;
                  animationContext4Status = AnimationStatus.start;
                });
                timesOfAnimationScriptOne = timesOfAnimationScriptOne + 1;
              }
            },
          ));
    } else {
      return Align(
        alignment: Alignment.topRight,
        child: Icon(
          Icons.favorite,
          color: Colors.grey,
          size: 100.0,
        ),
      );
    }
  }

  Widget starTwo() {
    if (animationContext2Status == AnimationStatus.start) {
      return Align(
        alignment: Alignment.topCenter,
        child: ScaleAnimation(
          duration: Duration(milliseconds: 500),
          child: Icon(
            Icons.favorite,
            color: Colors.grey,
            size: 100.0,
          ),
          onFinish: () {
            if(timesOfAnimationScriptOne == numberOfScore) {
              setAllFinishAnimationContextOneTwoThree(AnimationScript.one);
            } else {
              setState(() {
                animationContext1Status = AnimationStatus.finish;
                animationContext2Status = AnimationStatus.start;
              });
            }
            timesOfAnimationScriptOne = timesOfAnimationScriptOne + 1;
          },
        ),
      );
    } else {
      return Align(
        alignment: Alignment.topCenter,
        child: Icon(
          Icons.favorite,
          color: Colors.grey,
          size: 100.0,
        ),
      );
    }
  }

  Widget StarOneWithBounce() {
    if (getStatusOfAnimationContextOneTwoThreeIsFinished(AnimationScript.one) && timesOfAnimationScriptTwo <= numberOfScore) {
      if (animationContext4Status == AnimationStatus.start) {
        return Align(
            alignment: Alignment.topLeft,
            child: ScaleAnimation(
              duration: Duration(milliseconds: 500),
              delay: Duration(seconds: 0),
              upDown: false,
              child: Icon(
                Icons.favorite,
                color: Colors.purple,
                size: 100.0,
              ),
              onFinish: () {
                setState(() {
                  if(timesOfAnimationScriptTwo == numberOfScore) {
                    setAllFinishAnimationContextOneTwoThree(AnimationScript.two);
                  } else {
                    setState(() {
                      animationContext4Status = AnimationStatus.finish;
                      animationContext5Status = AnimationStatus.start;
                    });
                  }
                  timesOfAnimationScriptTwo = timesOfAnimationScriptTwo + 1;
                });
              },
            ));
      } else {
        return Align(
            alignment: Alignment.topLeft,
            child: Icon(
              Icons.favorite,
              color: Colors.purple,
              size: 100.0,
            ));
      }
    } else {
      return Opacity(opacity: 0.0);
    }
  }

  Widget StarTwoWithBounce() {
    print(timesOfAnimationScriptTwo);
    if (getStatusOfAnimationContextOneTwoThreeIsFinished(AnimationScript.one) &&
        animationContext4Status == AnimationStatus.finish && timesOfAnimationScriptTwo <= numberOfScore) {
      if (animationContext5Status == AnimationStatus.start) {
        return Align(
            alignment: Alignment.topCenter,
            child: ScaleAnimation(
              duration: Duration(milliseconds: 500),
              delay: Duration(seconds: 0),
              upDown: false,
              child: Icon(
                Icons.favorite,
                color: Colors.purple,
                size: 100.0,
              ),
              onFinish: () {
                if(timesOfAnimationScriptTwo == numberOfScore) {
                  setAllFinishAnimationContextOneTwoThree(AnimationScript.two);
                } else {
                  setState(() {
                    animationContext5Status = AnimationStatus.finish;
                    animationContext6Status = AnimationStatus.start;
                  });
                }
                timesOfAnimationScriptTwo = timesOfAnimationScriptTwo + 1;
              },
            ));
      } else {
        return Align(
            alignment: Alignment.topCenter,
            child: Icon(
              Icons.favorite,
              color: Colors.purple,
              size: 100.0,
            ));
      }
    } else {
      return Opacity(opacity: 0.0);
    }
  }

  Widget StarThreeWithBounce() {
    if (getStatusOfAnimationContextOneTwoThreeIsFinished(AnimationScript.one) &&
        animationContext5Status == AnimationStatus.finish && timesOfAnimationScriptTwo == 2) {
      if (animationContext6Status == AnimationStatus.start) {
        return Align(
            alignment: Alignment.topRight,
            child: ScaleAnimation(
              duration: Duration(milliseconds: 500),
              delay: Duration(seconds: 0),
              upDown: false,
              child: Icon(
                Icons.favorite,
                color: Colors.purple,
                size: 100.0,
              ),
              onFinish: () {
                if(timesOfAnimationScriptTwo == numberOfScore) {
                  setAllFinishAnimationContextOneTwoThree(AnimationScript.two);
                } else {
                  setState(() {
                    animationContext6Status = AnimationStatus.finish;
                    animationContext7Status = AnimationStatus.start;
                  });
                }
                timesOfAnimationScriptTwo = timesOfAnimationScriptTwo + 1;
              },
            ));
      } else {
        return Align(
            alignment: Alignment.topRight,
            child: Icon(
              Icons.favorite,
              color: Colors.purple,
              size: 100.0,
            ));
      }
    } else {
      return Opacity(opacity: 0.0);
    }
  }

  Widget StarOneWithFly() {
    if (getStatusOfAnimationContextOneTwoThreeIsFinished(AnimationScript.two)) {
      if (animationContext7Status == AnimationStatus.start) {
        return Positioned(
            child: CustomPositionedTransition(
                onFinish: () {
                  if(timesOfAnimationScriptThree == numberOfScore) {
                    setAllFinishAnimationContextOneTwoThree(AnimationScript.three);
                  } else {
                    setState(() {
                      animationContext7Status = AnimationStatus.finish;
                      animationContext8Status = AnimationStatus.start;
                    });
                  }
                  timesOfAnimationScriptThree = timesOfAnimationScriptThree + 1;
                },
                left: 300 - 55,
                top: 200 - 55,
                child: Align(
                  alignment: Alignment.topLeft,
                  // Delay để tạo hiệu ứng lần lượt
                  child: Icon(
                    Icons.favorite,
                    color: Colors.green,
                    size: 100.0,
                  ),
                )));
      } else {
        return Align(
          alignment: Alignment.topLeft,
          // Delay để tạo hiệu ứng lần lượt
          child: Icon(
            Icons.favorite,
            color: Colors.purple,
            size: 100.0,
          ),
        );
      }
    } else {
      return Opacity(opacity: 0.0);
    }
  }

  Widget StarTwoWithFly() {
    if(getStatusOfAnimationContextOneTwoThreeIsFinished(AnimationScript.two)) {
      if(animationContext8Status == AnimationStatus.start) {
        return Positioned(
            child : CustomPositionedTransition(
                onFinish: () {
                  if(timesOfAnimationScriptThree == numberOfScore) {
                    setAllFinishAnimationContextOneTwoThree(AnimationScript.three);
                  } else {
                    setState(() {
                      animationContext8Status = AnimationStatus.finish;
                      animationContext9Status = AnimationStatus.start;
                    });
                  }
                  timesOfAnimationScriptThree = timesOfAnimationScriptThree + 1;
                },
                left: 300 - 55,
                top: 200 - 55,
                child: Align(
                  alignment: Alignment.topCenter,
                  // Delay để tạo hiệu ứng lần lượt
                  child: Icon(
                    Icons.favorite,
                    color: Colors.green,
                    size: 100.0,
                  ),
                )));
      } else {
        return Align(
          alignment: Alignment.topCenter,
            child: Icon(
              Icons.favorite,
              color: Colors.purple,
              size: 100.0,
            ));
      }
    } else {
      return Opacity(opacity: 0.0);
    }
  }

  Widget StarThreeWithFly() {
    if(getStatusOfAnimationContextOneTwoThreeIsFinished(AnimationScript.two)) {
      if(animationContext9Status == AnimationStatus.start) {
        return Positioned(
            child : CustomPositionedTransition(
                onFinish: () {
                  setState(() {
                    if(timesOfAnimationScriptThree == numberOfScore) {
                      setAllFinishAnimationContextOneTwoThree(AnimationScript.three);
                    } else {
                      setState(() {
                        animationContext9Status = AnimationStatus.finish;
                      });
                    }
                    timesOfAnimationScriptThree = timesOfAnimationScriptThree + 1;
                  });
                },
                left: 300 - 55,
                top: 200 - 55,
                child: Align(
                  alignment: Alignment.topRight,
                  // Delay để tạo hiệu ứng lần lượt
                  child: Icon(
                    Icons.favorite,
                    color: Colors.green,
                    size: 100.0,
                  ),
                )));
      } else {
        return Positioned(
            child: Icon(
              Icons.favorite,
              color: Colors.grey,
              size: 100.0,
            ));
      }
    } else {
      return Opacity(opacity: 0.0);
    }
  }


  Widget StarOneFlyingBackGround() {
    if (getStatusOfAnimationContextOneTwoThreeIsFinished(AnimationScript.two)) {
      if (animationContext7Status == AnimationStatus.start ||
          animationContext7Status == AnimationStatus.finish) {
        return Align(
            alignment: Alignment.topLeft,
            child: Icon(
          Icons.favorite,
          color: Colors.grey,
          size: 100.0,
        ));
      } else {
        return Opacity(opacity: 0.0);
      }
    } else {
      return Opacity(opacity: 0.0);
    }
  }

  Widget StarOneWithBackgroundFinal() {
    if (getStatusOfAnimationContextOneTwoThreeIsFinished(AnimationScript.three) && numberOfScore >= 1) {
      if (animationContext10Status == AnimationStatus.start ) {
        return Align(
            alignment: Alignment.topLeft,
            child: Icon(
              Icons.favorite,
              color: Colors.purple,
              size: 100.0,
            ));
      } else {
        return Opacity(opacity: 0.0);
      }
    } else {
      return Opacity(opacity: 0.0);
    }
  }

  Widget StarTwoWithBackgroundFinal() {
    if (getStatusOfAnimationContextOneTwoThreeIsFinished(AnimationScript.three)  && numberOfScore >= 2) {
      if (animationContext10Status == AnimationStatus.start ) {
        return Align(
            alignment: Alignment.topCenter,
            child: Icon(
              Icons.favorite,
              color: Colors.purple,
              size: 100.0,
            ));
      } else {
        return Opacity(opacity: 0.0);
      }
    } else {
      return Opacity(opacity: 0.0);
    }
  }

  Widget StarTwoFlyingBackGround() {
    if (getStatusOfAnimationContextOneTwoThreeIsFinished(AnimationScript.two)) {
      if (animationContext8Status == AnimationStatus.start ||
          animationContext8Status == AnimationStatus.finish) {
        return Align(
          alignment: Alignment.topCenter,
            child: Icon(
              Icons.favorite,
              color: Colors.grey,
              size: 100.0,
            ));
      } else {
        return Opacity(opacity: 0.0);
      }
    } else {
      return Opacity(opacity: 0.0);
    }
  }

  Widget StarThreeFlyingBackGround() {
    if (getStatusOfAnimationContextOneTwoThreeIsFinished(AnimationScript.two)) {
      if (animationContext8Status == AnimationStatus.start ||
          animationContext8Status == AnimationStatus.finish) {
        return Align(
            alignment: Alignment.topCenter,
            child: Icon(
              Icons.favorite,
              color: Colors.grey,
              size: 100.0,
            ));
      } else {
        return Opacity(opacity: 0.0);
      }
    } else {
      return Opacity(opacity: 0.0);
    }
  }

  Widget StarThreeWithBackgroundFinal() {
    if (getStatusOfAnimationContextOneTwoThreeIsFinished(AnimationScript.three)  && numberOfScore >= 3) {
      if (animationContext10Status == AnimationStatus.start ) {
        return Align(
            alignment: Alignment.topCenter,
            child: Icon(
              Icons.favorite,
              color: Colors.purple,
              size: 100.0,
            ));
      } else {
        return Opacity(opacity: 0.0);
      }
    } else {
      return Opacity(opacity: 0.0);
    }
  }

  setAllFinishAnimationContextOneTwoThree(AnimationScript part) {
    switch (part) {
      case AnimationScript.one:
        setState(() {
          animationContext1Status = AnimationStatus.finish;
          animationContext2Status = AnimationStatus.finish;
          animationContext3Status = AnimationStatus.finish;
        });
        break;
      case AnimationScript.two:
        setState(() {
          animationContext4Status = AnimationStatus.finish;
          animationContext5Status = AnimationStatus.finish;
          animationContext6Status = AnimationStatus.finish;
        });
        break;
      case AnimationScript.three:
        setState(() {
          animationContext7Status = AnimationStatus.finish;
          animationContext8Status = AnimationStatus.finish;
          animationContext9Status = AnimationStatus.finish;
        });
        break;
      default:
        break;
    }
  }

  bool getStatusOfAnimationContextOneTwoThreeIsFinished(AnimationScript part) {
    switch (part) {
      case AnimationScript.one:
        return (animationContext1Status == AnimationStatus.finish &&
                animationContext2Status == AnimationStatus.finish &&
                animationContext3Status == AnimationStatus.finish)
            ? true
            : false;
      case AnimationScript.two:
        return (animationContext4Status == AnimationStatus.finish &&
                animationContext5Status == AnimationStatus.finish &&
                animationContext6Status == AnimationStatus.finish)
            ? true
            : false;
      case AnimationScript.three:
        return (animationContext7Status == AnimationStatus.finish &&
                animationContext8Status == AnimationStatus.finish &&
                animationContext9Status == AnimationStatus.finish)
            ? true
            : false;
      default:
        return false;
    }
  }

  Widget background(double position) {
    return Positioned(
        left: position,
        child: Icon(
          Icons.favorite,
          color: Colors.transparent,
          size: 100.0,
        ));
  }
}

class Square extends StatelessWidget {
  const Square({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100,
      height: 50,
      color: Colors.blueAccent,
    );
  }
}

class CustomPositionedTransition extends StatefulWidget {
  final Widget? child;
  final Duration? duration;
  final VoidCallback? onFinish;
  final Curve curve;
  final double? top;
  final double? left;
  final double? right;
  final Offset? position;
  final double? bottom;
  final Duration? delay;
  final Rect? rect;
  final Rect? rectTarget;

  CustomPositionedTransition(
      {key,
      required this.child,
      this.onFinish,
      this.duration = const Duration(milliseconds: 2000),
      this.delay = const Duration(milliseconds: 0),
      this.curve = Curves.linear,
      this.top,
      this.left,
      this.right,
      this.position,
      this.rect,
      this.bottom,
      this.rectTarget})
      : super(key: key ?? UniqueKey());

  @override
  _CustomPositionedTransitionState createState() =>
      _CustomPositionedTransitionState();
}

class _CustomPositionedTransitionState extends State<CustomPositionedTransition>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation<RelativeRect> _animation;
  late Animation<double> _animationScale;

  @override
  void initState() {
    super.initState();

    _animationController = AnimationController(
      duration: Duration(seconds: 3),
      vsync: this,
    );

    _animation = RelativeRectTween(
            end: RelativeRect.fromLTRB(widget.left!, widget.top!, 0, 0),
            begin: RelativeRect.fromLTRB(0, 0, 0, 0))
        .animate(
      CurvedAnimation(
        parent: _animationController,
        curve: widget.curve,
      ),
    );

    _animationScale = Tween<double>(begin: 1.0, end: 0.0).animate(
      CurvedAnimation(
        parent: _animationController,
        curve: Curves.easeInOut,
      ),
    );

    Future.delayed(widget.delay!, () {
      _animationController.forward().then((_) {
        widget.onFinish!();
      });
    });
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _animationController,
      builder: (context, child) {
        return PositionedTransition(
          rect: _animation,
          child: ScaleTransition(
            scale: _animationScale,
            child: widget.child,
          ),
        );
      },
    );
  }
}

class ScaleAnimation extends StatefulWidget {
  final Widget child;
  final Duration? duration;
  final Duration? delay;
  final double? initialScale;
  final double? finalScale;
  final VoidCallback? onFinish;
  final bool? upDown;

  ScaleAnimation({
    key,
    required this.child,
    this.duration = const Duration(milliseconds: 300),
    this.delay = const Duration(milliseconds: 0),
    this.upDown = true,
    this.onFinish,
    this.initialScale = 1.0,
    this.finalScale = 0.5,
  }) : super(key: key);

  @override
  _ScaleAnimationState createState() => _ScaleAnimationState();
}

class _ScaleAnimationState extends State<ScaleAnimation>
    with SingleTickerProviderStateMixin {
  late Animation<double> animationInc;
  late Animation<double> animationDec;
  late AnimationController _animationController;
  late Animation<double> _animationScale;
  bool _isVisible = false;

  @override
  void initState() {
    super.initState();

    Timer(widget.delay!, () {
      setState(() {
        _isVisible = true;
      });
    });

    _animationController = AnimationController(
      duration: widget.duration,
      vsync: this,
    );

    if (widget.upDown!) {
      animationInc = Tween<double>(begin: 1, end: 0.5).animate(CurvedAnimation(
          parent: _animationController,
          curve: Interval(0, 0.5, curve: Curves.easeOut))); // Curves.easeOut

      animationDec = Tween<double>(begin: 0.5, end: 1).animate(CurvedAnimation(
          parent: _animationController,
          curve: Interval(0.5, 1, curve: Curves.easeIn))); // Curves.easeIn
    } else {
      animationInc = Tween<double>(begin: 1, end: 1.5).animate(CurvedAnimation(
          parent: _animationController,
          curve: Interval(0, 0.5, curve: Curves.easeOut))); // Curves.easeOut

      animationDec = Tween<double>(begin: 1.5, end: 1).animate(CurvedAnimation(
          parent: _animationController,
          curve: Interval(0.5, 1, curve: Curves.easeIn))); // Curves.easeIn
    }

    Future.delayed(widget.delay!, () {
      _animationController.forward().then((_) {
        if (widget.onFinish != null) {
          widget.onFinish!();
        }
      });
    });
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
        animation: _animationController,
        builder: (context, child) {
          return Transform.scale(
            scale: (_animationController.value < 0.5)
                ? animationInc.value
                : animationDec.value,
            child: child,
          );
        },
        child: widget.child);
  }
}
