import 'dart:async';
import 'dart:io';
import 'dart:ui' as ui;
import 'dart:ui';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/animation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/semantics.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'dart:developer';

import 'package:permission_handler/permission_handler.dart';


class MyAppPaint extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Canvas Decorator',
      home: CanvasDecoratorScreen(),
    );
  }
}

class CanvasDecoratorScreen extends StatefulWidget {
  @override
  _CanvasDecoratorScreenState createState() => _CanvasDecoratorScreenState();
}

class _CanvasDecoratorScreenState extends State<CanvasDecoratorScreen> {
  bool drawingBlocked = false;
  bool earseIsSelected = false;
  List<Point> points = [];
  List<List<Point>> ListOfPoints = [];
  List<MySticker> stickerList = [];
  GlobalKey _canvasKey = GlobalKey();
  List<ui.Image> stickerImageList = [];
  List<String> imageLocalList = ['graphic-tablet.png', 'learning.png', 'verified.png'];
  dynamic image = null;

  Future<ui.Image> loadImage(String imageName) async {
    final data = await rootBundle.load("assets/images/${imageName}");
    return decodeImageFromList(data.buffer.asUint8List());
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadSticker();
  }

  void _eraseDrawing() {
    setState(() {
      earseIsSelected = !earseIsSelected;
    });
  }

  checkPermission() async {
    PermissionStatus result;
    if (Platform.isAndroid) {
      DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      if (androidInfo.version.sdkInt >= 33) {
        result = await Permission.photos.request();
      } else {
        result = await Permission.storage.request();
      }
      if (result.isGranted) {
        _saveToGallery();
      }
    } else {
      _saveToGallery();
    }
  }

  _saveToGallery() async {
    RenderRepaintBoundary boundary =
    _canvasKey.currentContext!.findRenderObject() as RenderRepaintBoundary;
    ui.Image image = await boundary.toImage();
    ByteData? byteData =
    await (image.toByteData(format: ui.ImageByteFormat.png));
    if (byteData != null) {
      final result =
      await ImageGallerySaver.saveImage(byteData.buffer.asUint8List());
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('Image saved to gallery.'),
        ),
      );
    }
  }

  loadSticker() {
    imageLocalList.forEach((sticker) async {
       image = await loadImage(sticker);
       stickerImageList.add(image);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: DragTarget(
        onAcceptWithDetails: (details)  {
          setState(() {
            final mySticker = MySticker(details.offset, stickerImageList[details.data as int]);
            stickerList = List.of(stickerList)..add(mySticker);
          });
        },
        builder: (BuildContext context, List<Object?> candidateData, List<dynamic> rejectedData) {
          return SafeArea(
        child:Column(
          children: [
            Container(
              color: Colors.white,
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height - 300,
              child: InteractiveViewer(
                child: AbsorbPointer(
                  absorbing: drawingBlocked,
                  child: Listener(
                    onPointerDown: (e) {
                      points.clear();
                    },
                    onPointerMove: (details) {
                      RenderBox renderBox = context!.findRenderObject()! as RenderBox;
                      Offset cursorLocation = renderBox.localToGlobal(details.localPosition);
                      setState(() {
                        points = List.of(points)..add(Point(cursorLocation, (earseIsSelected ? Colors.white : Colors.black) ));
                        ListOfPoints.add(points);
                      });
                    },
                    child: RepaintBoundary(
                      key: _canvasKey,
                      child: CustomPaint(
                          painter: CanvasPainter(ListOfPoints,stickerList),
                          size: Size.infinite
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: earseIsSelected  ? Colors.greenAccent : Colors.white
                    ),
                    onPressed: _eraseDrawing,
                    child: Text('Erase'),
                  ),
                  ElevatedButton(
                    onPressed: (){
                      checkPermission();
                    },
                    child: Text('Save'),
                  ),
                ],
              ),
            ),
            Expanded(
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: imageLocalList.length,
                itemBuilder: (context, index) {
                  return Draggable(
                    data: index ,
                    child: Image.asset('assets/images/${imageLocalList[index]}',width: 150),
                    feedback: Image.asset('assets/images/${imageLocalList[index]}', width: 100),
                  );
                },
              ),
            ),
          ],
        ),
      ); },

      ),
    );
  }
}

class CanvasPainter extends CustomPainter {
  CanvasPainter(this.Points, this.stickerList);
  List<MySticker> stickerList;
  List<List<Point>> Points;

  Paint paintBrush = Paint()
    ..color = Colors.black
    ..strokeWidth = 5
    ..strokeJoin = StrokeJoin.round;

  @override
  Future<void> paint(Canvas canvas, Size size) async {
    stickerList.forEach((sticker) {
      canvas.drawImage(sticker.image, sticker.offset, Paint());
    });
    Points.forEach((points) {
      for (int i = 0; i < points.length - 1; i++) {
        canvas.drawLine(points[i].offset, points[i + 1].offset, paintBrush..color = (points[i] as Point).color as ui.Color );
      }
    });
  }

  @override
  bool shouldRepaint(CanvasPainter oldDelegate) {
   return true;
  }
}

class Point{
  Color color;
  Offset offset;
  Point(this.offset, this.color);
}

class MySticker {
  Offset offset;
  ui.Image image;
  MySticker(this.offset,this.image);
}
